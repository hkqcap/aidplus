﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace AidPlus
{
    [Register ("FirstViewController")]
    partial class FirstViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView noteSubView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (noteSubView != null) {
                noteSubView.Dispose ();
                noteSubView = null;
            }
        }
    }
}