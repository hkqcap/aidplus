using Foundation;
using System;
using UIKit;
using System.IO;

namespace AidPlus
{
    public partial class MarketStructureViewController : UIViewController
    {
        public MarketStructureViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.AddSubview(webView);
            webView.ScalesPageToFit = true;
            var URL = "http://hkqcap.no-ip.org/aidplus_content/market_structure.html";
            webView.LoadRequest(new NSUrlRequest(new NSUrl(URL)));
        }
    }
}