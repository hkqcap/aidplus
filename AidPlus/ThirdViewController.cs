﻿using Foundation;
using System;
using UIKit;

namespace AidPlus
{
    public partial class ThirdViewController : UIViewController
    {
        public ThirdViewController(IntPtr handle) : base(handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            View.AddSubview(webView2);
            webView2.ScalesPageToFit = true;
            var URL = "https://phet.colorado.edu/sims/html/projectile-motion/latest/projectile-motion_en.html";
            webView2.LoadRequest(new NSUrlRequest(new NSUrl(URL)));
        }
    }
}