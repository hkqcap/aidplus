﻿using System;

using UIKit;
using AVFoundation;
using Foundation;
using CoreGraphics;
using System.Drawing;

namespace AidPlus
{
    public partial class FirstViewController : UIViewController
    {
        protected FirstViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Perform any additional setup after loading the view, typically from a nib.
            //Set the movie file path
            string moviePath = NSBundle.MainBundle.PathForResource("businesscycle2", "mov");
            NSUrl movieUrl = NSUrl.FromFilename(moviePath);

            //Using AVPlayer(using AVFoundation)
            AVPlayer avPlayer;
            AVPlayerLayer playerLayer;
            AVAsset asset;
            AVPlayerItem playerItem;
            asset = AVAsset.FromUrl(movieUrl);
            playerItem = new AVPlayerItem(asset);
            avPlayer = new AVPlayer(playerItem);
            playerLayer = AVPlayerLayer.FromPlayer(avPlayer);
            playerLayer.Frame = new CGRect(16, 40, 349, 300);
            View.Layer.AddSublayer(playerLayer);
            avPlayer.Play();

            UIWebView noteWebView = new UIWebView(new CGRect(16,300,349,300));
            View.AddSubview(noteWebView);
            noteWebView.ScalesPageToFit = true;
            var URL = "http://hkqcap.no-ip.org/aidplus_content/business_cycle.html";
            noteWebView.LoadRequest(new NSUrlRequest(new NSUrl(URL)));
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
